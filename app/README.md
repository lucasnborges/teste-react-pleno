## Instruções

Clone localmente o projeto

```sh
git clone https://lucasnborges@bitbucket.org/lucasnborges/teste-react-pleno.git
```

No terminal, entre na pasta **app** onde encontram-se os arquivos do projeto

```sh
cd app
```

Adicione o arquivo **local.properties** na pasta android

Dentro do arquivo **local.properties** adicione o diretório do seu sdk

```sh
sdk.dir=C:\\Users\\Lucas\\AppData\\Local\\Android\\Sdk
```

Faça a instalação de todas as dependencias

```sh
npm install
```

Em um ambiente android configurado e com um emulador ou celular,
rode o seguinte comando:

```sh
npm run android
```
