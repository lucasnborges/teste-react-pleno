import axios from 'axios'

export const getAddress = async cep => {
  let { data } = await axios.get(`https://viacep.com.br/ws/${cep}/json/`)

  const address = {
    street: data.logradouro,
    district: data.bairro,
    city: data.localidade,
    state: data.uf
  }

  return address
}

module.exports = {
  getAddress
}
