import React from 'react'
import { StyleSheet } from 'react-native'
import { Icon, TopNavigation, TopNavigationAction } from '@ui-kitten/components'

const BackIcon = props => <Icon {...props} name="arrow-back" />

const Header = props => {
  function BackAction() {
    return <TopNavigationAction icon={BackIcon} onPress={handleGoBack} />
  }

  function handleGoBack() {
    props.navigation.goBack()
  }

  return (
    <TopNavigation
      title={props.title}
      style={styles.header}
      accessoryLeft={BackAction}
    />
  )
}

const styles = StyleSheet.create({
  header: {
    elevation: 3
  }
})

export default Header
