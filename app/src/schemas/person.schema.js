export default class PersonSchema {
  static schema = {
    name: 'Person',
    primaryKey: 'id',
    properties: {
      id: {
        type: 'int',
        indexed: true
      },
      name: 'string',
      age: 'string',
      cpf: 'string',
      rg: 'string',
      cep: 'string',
      street: 'string',
      number: 'string',
      district: 'string',
      city: 'string',
      state: 'string'
    }
  }
}
