import Realm from 'realm'

import PersonSchema from '../schemas/person.schema'

export default function RealmDB() {
  return Realm.open({
    schema: [PersonSchema]
  })
}
