import React from 'react'
import { StyleSheet, View, ScrollView, Alert } from 'react-native'
import { Formik } from 'formik'
import { Layout, Input, Button } from '@ui-kitten/components'
import { connect, useDispatch } from 'react-redux'
import { fetchDataPerson } from '../../stores/actions/person.action'
import { getAddress } from '../../api/address.api'
import RealmDB from '../../services/realm'
import Header from '../../components/Header/Header.component'

const schema = [
  {
    prop: 'name',
    placeholder: 'Nome'
  },
  {
    prop: 'age',
    placeholder: 'Idade',
    type: 'numeric',
    maxLength: 3
  },
  {
    prop: 'cpf',
    placeholder: 'CPF',
    type: 'numeric',
    maxLength: 11
  },
  {
    prop: 'rg',
    placeholder: 'RG',
    type: 'numeric'
  }
]

const address = [
  {
    prop: 'street',
    placeholder: 'Rua'
  },
  {
    prop: 'number',
    placeholder: 'Número',
    type: 'numeric'
  },
  {
    prop: 'district',
    placeholder: 'Bairro'
  },
  {
    prop: 'city',
    placeholder: 'Cidade'
  },
  {
    prop: 'state',
    placeholder: 'Estado'
  }
]

const initialSchemaValues = {
  name: '',
  age: '',
  cpf: '',
  rg: '',
  cep: '',
  street: '',
  number: '',
  district: '',
  city: '',
  state: ''
}

const RegisterPerson = ({ navigation }) => {
  const dispatch = useDispatch()

  async function handleAddPerson(values, { resetForm }) {
    Alert.alert('Pessoa cadastrada', JSON.stringify(values), [
      {
        text: 'Confirmar'
      }
    ])

    const realm = await RealmDB()
    const persons = realm.objects('Person')

    values.id = persons.length + Math.floor(Math.random() * 999)

    realm.write(() => {
      realm.create('Person', values)
    })

    dispatch(fetchDataPerson())
    resetForm()
  }

  async function handleChangeCep(cep, setFieldValue) {
    setFieldValue('cep', cep)

    if (cep.length === 8) {
      const resAddress = await getAddress(cep)

      setFieldValue('city', resAddress.city)
      setFieldValue('state', resAddress.state)
      setFieldValue('street', resAddress.street)
      setFieldValue('district', resAddress.district)
    }
  }

  return (
    <>
      <Header title="Cadastrar pessoa" navigation={navigation} />
      <Layout style={styles.layout}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={styles.contentContainer}
          showsVerticalScrollIndicator={false}>
          <Formik
            initialValues={initialSchemaValues}
            onSubmit={handleAddPerson}>
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              setFieldValue
            }) => (
              <View>
                {schema.map(s => (
                  <Input
                    key={s.prop}
                    returnKeyType="next"
                    style={styles.input}
                    value={values[s.prop]}
                    maxLength={s.maxLength}
                    placeholder={s.placeholder}
                    onBlur={handleBlur(s.prop)}
                    keyboardType={s.type || 'default'}
                    onChangeText={handleChange(s.prop)}
                  />
                ))}
                <Input
                  key={'cep'}
                  maxLength={8}
                  placeholder="CEP"
                  value={values.cep}
                  style={styles.input}
                  returnKeyType="next"
                  keyboardType="numeric"
                  onChangeText={value => handleChangeCep(value, setFieldValue)}
                />
                {address.map(s => (
                  <Input
                    key={s.prop}
                    returnKeyType="next"
                    style={styles.input}
                    value={values[s.prop]}
                    maxLength={s.maxLength}
                    placeholder={s.placeholder}
                    onBlur={handleBlur(s.prop)}
                    keyboardType={s.type || 'default'}
                    onChangeText={handleChange(s.prop)}
                  />
                ))}

                <Button onPress={handleSubmit} title="Submit">
                  Cadastrar
                </Button>
              </View>
            )}
          </Formik>
        </ScrollView>
      </Layout>
    </>
  )
}

const styles = StyleSheet.create({
  layout: {
    flex: 1
  },
  contentContainer: {
    padding: 24
  },
  input: {
    marginVertical: 4
  }
})

const mapStateToProps = state => {
  return {
    list: state.personReducer.persons
  }
}

export default connect(mapStateToProps, null)(RegisterPerson)
