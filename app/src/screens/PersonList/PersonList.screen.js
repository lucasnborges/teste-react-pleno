import React, { useState, useEffect } from 'react'
import { View } from 'react-native'
import { connect, useDispatch } from 'react-redux'
import { fetchDataPerson } from '../../stores/actions/person.action'
import {
  Button,
  Divider,
  Icon,
  List,
  ListItem,
  TopNavigation,
  Text,
  Modal,
  Card,
  Spinner
} from '@ui-kitten/components'
import RealmDB from '../../services/realm'
import styles from './PersonList.styles'

const AddPersonIcon = props => <Icon {...props} name="plus" />
const EditIcon = props => <Icon {...props} name="edit-outline" />
const TrashIcon = props => <Icon {...props} name="trash-outline" />

const PersonList = ({ navigation, list }) => {
  const dispatch = useDispatch()

  const [persons, setPersons] = useState([])
  const [selected, setSelected] = useState({})
  const [isLoading, setLoading] = useState(false)
  const [modalVisible, setModalVisible] = useState(false)

  async function loadPersonList() {
    const realm = await RealmDB()
    const data = realm.objects('Person')

    setPersons(data)
  }

  useEffect(() => {
    loadPersonList()
    dispatch(fetchDataPerson())
  }, [dispatch])

  const renderAddPersonAction = () => (
    <Button
      size="small"
      status="primary"
      onPress={handleGoRegister}
      accessoryLeft={AddPersonIcon}
    />
  )

  const renderItemAccessory = person => (
    <>
      <Button
        size="small"
        status="basic"
        appearance="ghost"
        accessoryLeft={EditIcon}
      />
      <Button
        size="small"
        status="basic"
        appearance="ghost"
        accessoryLeft={TrashIcon}
        onPress={() => handleOpenModal(person)}
      />
    </>
  )

  const renderItemIcon = props => <Icon {...props} name="person" />

  const renderItem = ({ item, index }) => (
    <>
      <ListItem
        key={'person-list-item-' + index}
        title={`${item.name}`}
        accessoryLeft={renderItemIcon}
        description={`${item.age} anos`}
        accessoryRight={() => renderItemAccessory(item)}
      />
      <Divider />
    </>
  )

  const renderTitle = () => (
    <View style={styles.titleContainer}>
      <Icon
        fill="#8F9BB3"
        name="file-text-outline"
        style={styles.navbarLeftIcon}
      />
      <Text style={styles.navbarLeftText}>Lista de pessoas cadastradas</Text>
    </View>
  )

  const Footer = props => (
    <View style={[props.style, styles.footerContainer]}>
      <Button
        size="small"
        status="basic"
        style={styles.footerControl}
        onPress={() => setModalVisible(false)}>
        CANCELAR
      </Button>
      <Button
        size="small"
        style={styles.footerControl}
        onPress={() => handleDeleteItem(selected.id)}>
        {isLoading ? <Spinner size="tiny" status="basic" /> : 'CONFIRMAR'}
      </Button>
    </View>
  )

  function handleGoRegister() {
    navigation.navigate('RegisterPerson')
  }

  function handleOpenModal(person) {
    setSelected(person)
    setModalVisible(true)
  }

  async function handleDeleteItem(id) {
    setLoading(true)
    setPersons(persons.filter(p => p.id !== id))

    const realm = await RealmDB()

    realm.write(() => {
      realm.delete(realm.objectForPrimaryKey('Person', id))
    })

    setTimeout(() => {
      setModalVisible(false)
      setLoading(false)
    }, 1500)
  }

  return (
    <React.Fragment>
      <Modal visible={modalVisible} backdropStyle={styles.backdrop}>
        <Card disabled footer={Footer} style={styles.card}>
          <Text style={styles.modalText}>
            tem certeza que deseja <Text status="danger">excluir</Text> o item
            da sua lista de cadastro?
          </Text>
        </Card>
      </Modal>
      <TopNavigation
        title={renderTitle}
        style={styles.navigationBar}
        accessoryRight={renderAddPersonAction}
      />
      <List style={styles.container} data={list} renderItem={renderItem} />
    </React.Fragment>
  )
}

const mapStateToProps = state => {
  return {
    list: state.personReducer.persons
  }
}

export default connect(mapStateToProps, null)(PersonList)
