import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  navigationBar: {
    elevation: 3
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 4
  },
  navbarLeftIcon: {
    width: 24,
    height: 24
  },
  navbarLeftText: {
    marginLeft: 4,
    color: '#8F9BB3',
    fontWeight: 'bold'
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.2)'
  },
  modalText: {
    lineHeight: 20
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  footerControl: {
    marginHorizontal: 2
  },
  card: {
    marginHorizontal: 32
  }
})
