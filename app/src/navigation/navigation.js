import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import PersonList from '../screens/PersonList/PersonList.screen'
import RegisterPerson from '../screens/RegisterPerson/RegisterPerson.screen'

const Stack = createStackNavigator()

const MainNavigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">
        <Stack.Screen name="PersonList" component={PersonList} />
        <Stack.Screen name="RegisterPerson" component={RegisterPerson} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default MainNavigation
