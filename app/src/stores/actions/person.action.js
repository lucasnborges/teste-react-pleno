import RealmDB from '../../services/realm'

export const fetchPersonRequest = () => {
  return {
    type: 'FETCH_PERSON_REQUEST'
  }
}

export const fetchPersonSuccess = persons => {
  return {
    type: 'FETCH_PERSON_SUCCESS',
    payload: persons
  }
}

export const fetchPersonFail = () => {
  return {
    type: 'FETCH_PERSON_FAILED'
  }
}

export const fetchDataPerson = () => async dispatch => {
  try {
    dispatch(fetchPersonRequest())
    const realm = await RealmDB()
    const data = realm.objects('Person')
    dispatch(fetchPersonSuccess(data))
  } catch (error) {
    dispatch(fetchPersonFail())
  }
}
