const initialState = {
  persons: [],
  isLoading: false
}

export const personReducer = (state = initialState, action) => {
  const { payload } = action
  switch (action.type) {
    case 'FETCH_PERSON_REQUEST':
      return {
        ...state,
        isLoading: true
      }

    case 'FETCH_PERSON_SUCCESS':
      return {
        ...state,
        persons: payload,
        isLoading: false
      }
    case 'FETCH_PERSON_FAILED':
      return {
        ...state,
        isLoading: false
      }

    default:
      return state
  }
}

export default personReducer
