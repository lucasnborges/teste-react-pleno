import { combineReducers } from 'redux'
import personReducer from './person.reducer'

const reducers = combineReducers({
  personReducer
})

export default reducers
